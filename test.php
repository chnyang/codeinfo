<?php

require 'vendor/autoload.php';

use PHPQRCode\QRcode;

function qr_code($data = '', $matrixPointSize = 2, $errorCorrectionLevel = 'L')
{
    ob_start();
    // 输出图像
    QRcode::png($data, false, $errorCorrectionLevel, $matrixPointSize, 2);
    $content = ob_get_clean();
    return $content;
}

/**
 * 分享图片生成
 * @param array $data
 * @param string $fileName 保存文件名,默认空则直接输入图片
 */
function createSharePng($data = [], $fileName = '')
{
    //创建画布
    $im = imagecreatetruecolor(400, 200);

    //填充画布背景色
    $bgColor = imagecolorallocate($im, 255, 255, 255);
    imagefill($im, 0, 0, $bgColor);

    //字体文件
    $font_file = "consya.ttf";

    //设定字体的颜色
    $color = ImageColorAllocate($im, 0, 0, 0);

    //标题
    imagettftext($im, 12, 0, 170, 20, $color, $font_file, '科室名称');

    //二维码
    $codeImg = imagecreatefromstring(qr_code('1321101223344'));
    imagecopyresized($im, $codeImg, 5, 40, 0, 0, 170, 170, 100, 100);
    $x1 = 90;
    $x2 = 230;
    imagettftext($im, 10, 0, $x1, 50, $color, $font_file, '设备名称');
    imagettftext($im, 10, 0, $x1, 70, $color, $font_file, '设备编号');
    imagettftext($im, 10, 0, $x1, 90, $color, $font_file, '规格型号');
    imagettftext($im, 10, 0, $x1, 110, $color, $font_file, '启用日期');
    imagettftext($im, 10, 0, $x1, 130, $color, $font_file, '使用部门');
    imagettftext($im, 10, 0, $x2, 90, $color, $font_file, '维修电话');
    imagettftext($im, 10, 0, $x2, 110, $color, $font_file, '存放地点');
    imagettftext($im, 10, 0, $x2, 130, $color, $font_file, '保修期限');

    $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
    try {
        $data = $generator->getBarcode('1321101237312', $generator::TYPE_CODE_128);
        $codeImg = imagecreatefromstring($data);
        imagecopyresized($im, $codeImg, 90, 150, 0, 0, 170, 170, 100, 100);
    } catch (\Picqer\Barcode\Exceptions\BarcodeException $e) {
    }



    //输出图片
    if ($fileName) {
        imagepng($im, $fileName);
    } else {
        Header("Content-Type: image/png");
        imagepng($im);
    }

    //释放空间
    imagedestroy($im);
    imagedestroy($codeImg);
}

/**
 * 从图片文件创建Image资源
 * @param mixed $file 图片文件，支持url
 * @return bool|resource    成功返回图片image资源，失败返回false
 */
function createImageFromFile($file)
{
    $fileSuffix = pathinfo($file, PATHINFO_EXTENSION);
//    if (preg_match('/http(s)?:\/\//', $file)) {
//        $fileSuffix = getNetworkImgType($file);
//    } else {
//    }

    if (!$fileSuffix) return false;

    switch ($fileSuffix) {
        case 'jpeg':
            $theImage = @imagecreatefromjpeg($file);
            break;
        case 'jpg':
            $theImage = @imagecreatefromjpeg($file);
            break;
        case 'png':
            $theImage = @imagecreatefrompng($file);
            break;
        case 'gif':
            $theImage = @imagecreatefromgif($file);
            break;
        default:
            $theImage = @imagecreatefromstring(file_get_contents($file));
            break;
    }

    return $theImage;
}


/**
 * 分行连续截取字符串
 * @param string $str 需要截取的字符串,UTF-8
 * @param int $row 截取的行数
 * @param int $number 每行截取的字数，中文长度
 * @param bool $suffix 最后行是否添加‘...’后缀
 * @return array    返回数组共$row个元素，下标1到$row
 */
function cn_row_substr($str, $row = 1, $number = 10, $suffix = true)
{
    $result = array();
    for ($r = 1; $r <= $row; $r++) {
        $result[$r] = '';
    }

    $str = trim($str);
    if (!$str) return $result;

    $theStrlen = strlen($str);

    //每行实际字节长度
    $oneRowNum = $number * 3;
    for ($r = 1; $r <= $row; $r++) {
        if ($r == $row and $theStrlen > $r * $oneRowNum and $suffix) {
            $result[$r] = mg_cn_substr($str, $oneRowNum - 6, ($r - 1) * $oneRowNum) . '...';
        } else {
            $result[$r] = mg_cn_substr($str, $oneRowNum, ($r - 1) * $oneRowNum);
        }
        if ($theStrlen < $r * $oneRowNum) break;
    }

    return $result;
}

/**
 * 按字节截取utf-8字符串
 * 识别汉字全角符号，全角中文3个字节，半角英文1个字节
 * @param string $str 需要切取的字符串
 * @param int $len 截取长度[字节]
 * @param int $start 截取开始位置，默认0
 * @return string
 */
function mg_cn_substr($str, $len, $start = 0)
{
    $q_str = '';
    $q_strlen = ($start + $len) > strlen($str) ? strlen($str) : ($start + $len);

    //如果start不为起始位置，若起始位置为乱码就按照UTF-8编码获取新start
    if ($start and json_encode(substr($str, $start, 1)) === false) {
        for ($a = 0; $a < 3; $a++) {
            $new_start = $start + $a;
            $m_str = substr($str, $new_start, 3);
            if (json_encode($m_str) !== false) {
                $start = $new_start;
                break;
            }
        }
    }

    //切取内容
    for ($i = $start; $i < $q_strlen; $i++) {
        //ord()函数取得substr()的第一个字符的ASCII码，如果大于0xa0的话则是中文字符
        if (ord(substr($str, $i, 1)) > 0xa0) {
            $q_str .= substr($str, $i, 3);
            $i += 2;
        } else {
            $q_str .= substr($str, $i, 1);
        }
    }
    return $q_str;
}


//使用方法-------------------------------------------------
//数据格式，如没有优惠券coupon_price值为0。
$gData = [
    'pic' => 'ff.png',
    'title' => 'chic韩版工装羽绒棉服女冬中长款2017新款棉袄大毛领收腰棉衣外套',
    'price' => 19.8,
    'original_price' => 119.8,
    'coupon_price' => 100
];
//直接输出
//createSharePng($gData, 'code_png/php_code.jpg');
//输出到图片
createSharePng($data, 'share.png');

